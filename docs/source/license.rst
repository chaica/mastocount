License
=======

This software comes under the terms of the **GPLv3+**. See the LICENSE file for the complete history of the license and the full text of the past and current licenses.
