Documentation for the Remindr project
=====================================

Mastocount takes several [Mastodon](https://joinmastodon.org) accounts credentials, retrieve the number of followers of these accounts and print detailed numbers and the total number.

Guide
=====

.. toctree::
   :maxdepth: 2

   install
   configure
   use
   license
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

